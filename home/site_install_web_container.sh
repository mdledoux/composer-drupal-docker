#!/bin/bash
#source ~/.env


#cp /var/www/bashrc.sh ~/.bashrc
mv ~/composer_bashrc ~/.bashrc

DB_USER=$POSTGRES_USER$MYSQL_USER
DB_PASS=$POSTGRES_PASSWORD$MYSQL_PASSWORD
DB_NAME=$POSTGRES_DB$MYSQL_DATABASE
#DB_HOST=$POSTGRES_HOST$MYSQL_HOST  #always db within container network
#DB_PORT=$POSTGRES_PORT$MYSQL_PORT  #always 5432/3306 within container network

DB_STRING_SQLITE="sqlite://sites/default/database/.ht.sqlite"


# DB_USER=$POSTGRES_USER$MYSQL_USER
# DB_PASS=$POSTGRES_PASSWORD$MYSQL_PASSWORD
# DB_NAME=$POSTGRES_DB$MYSQL_DATABASE
# DB_HOST=$POSTGRES_HOST$MYSQL_HOST
# DB_PORT=$POSTGRES_PORT$MYSQL_PORT

#DB_HOST=drupal_demo_db
if [ "$DB_DRVR" == "sqlite" ]; then
	DB_STRING=$DB_STRING_SQLITE
else
	DB_STRING=$DB_DRVR://$DB_USER:$DB_PASS@$DB_HOST:$DB_PORT/$DB_NAME
fi
echo "DB_STRING: $DB_STRING"

bash ~/links.sh


#mkdir -p drupal-project/web/sites/default/database 
drush site-install \
	--account-pass $ADMIN_PASS \
	--site-name="$SITE_NAME" \
	--db-url=$DB_STRING -y

#	--db-url=pgsql://root:pass@localhost:port/dbname -y
#	--db-url=mysql://root:pass@localhost:port/dbname -y
#	--db-url=sqlite://sites/default/database/.ht.sqlite -y 

chmod -R 777 sites/default/files
chown 1000:www-data sites/default/files

drush cr
