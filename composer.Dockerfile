FROM composer:latest as composer-drupal-custom

LABEL maintainer="jason@ciandt.com"

#RUN echo "@edge http://nl.alpinelinux.org/alpine/edge/main" >> /etc/apk/repositories
RUN apk update
RUN apk upgrade --update && apk add \
    libjpeg-turbo-dev \
    libpng-dev \
    postgresql-libs \
    postgresql-dev \
    postgresql \
    mysql-client \
    libpq 
#    libpq-dev \
RUN apk add \
    libzip \
    libzip-dev \
    libxml2
#RUN apk add \
#    libfreetype6-dev
#RUN docker-php-ext-configure gd --with-freetype --with-jpeg \
RUN docker-php-ext-configure gd --with-jpeg \
    && docker-php-ext-install gd pgsql pdo pdo_mysql pdo_pgsql zip

