#!/bin/bash
source .env


#mkdir -p drupal-project/web/sites/default/database 
drush site-install \
	--account-pass iamroot \
	--site-name="$SITE_NAME" \
	--db-url=sqlite://sites/default/database/.ht.sqlite -y 

echo $PG_DBNAME

sudo chown 1000:www-data drupal-project/web/sites/default/database
sudo chmod 777 drupal-project/web/sites/default/database

sudo chown 1000:www-data drupal-project/web/sites/default/files
sudo chmod -R 777 drupal-project/web/sites/default/files

sudo chown www-data:www-data  drupal-project/web/sites/default/database/.ht.sqlite 
