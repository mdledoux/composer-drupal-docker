#!/bin/bash
sudo echo
echo "RUNNING site-backup.sh !!!!"
source env.include.sh
source $BASE_PATH/.env

CMD=$(basename "$0")
TOOL_PATH=${0//bin\/$CMD/}
TOOL_PATH=${0//$CMD/}
# THESE ABOVE ARE NECESSAY BECAUSE env.include.sh IS NOT SOURCING...

HASH=`git rev-parse HEAD`
DATE=`date +'%Y-%m-%d.%H_%M_%S'`
PROJECT_NAME=$(basename "$PWD")
BACKUP_STAMP=$PROJECT_NAME--$DATE--$HASH


if [ -z "$1" ]; then
  echo "You must specify a backup location:"
  echo "  Usage:  $0 /web/backups"
  echo "ONLY ABSOLUTE PATHS ALLOWED - AVOID THIS:"
  echo "  Usage:  $0 ../backups"
  exit 1
else
  BACKUPS_DIR=$1
  echo "BACKUPS DIR: $1"
  #exit 0
fi

echo "DB BACKUP FILES STAMP:"
echo "               $BACKUP_STAMP"
echo

echo "TOOL_PATH: $TOOL_PATH"

# some good logic to use:
# mkdir $1 && cd $1 || exit 1


# BACKUP PHASE
echo "CREATING BACKUP DIR IF IT DOES NOT EXIST"
mkdir -p $BACKUPS_DIR || exit 1


##### START BACKUP
#BACKUP ENTIRE drupal-project ROOT DIR FIRST
echo "BACKING UP ENTIRE PROJECT FOLDER"
#zip -rqy  $BACKUPS_DIR/$BACKUP_STAMP\.drupal-project.zip   ../$PROJECT_NAME
tar cfzp  $BACKUPS_DIR/$BACKUP_STAMP\.drupal-project.tar.gz   ../$PROJECT_NAME

#$DRUPAL dbdu --gz --file=../backups/$BACKUP_STAMP\.db.gz   default
# NOT WORKING - only 154 Bytes:  $DRUPAL dbdu  --file=../backups/$BACKUP_STAMP\.db.sql   default


echo "BACKING UP DATABASE -- you may be prompted for the DB password"
#BACKUP_CMD="$DRUSH sql-dump --result-file=$BACKUPS_DIR/$BACKUP_STAMP\.db.sql   ||  exit 1"
BACKUP_CMD="BAK_FILE_PATH=$BACKUPS_DIR  $DRUSH sql-dump --result-file=/backups/$BACKUP_STAMP\.db.sql   ||  exit 1"
echo $BACKUP_CMD
eval $BACKUP_CMD || exit 1

cd $BASE_PATH/$DRUPAL_PROJECT
echo "BACKING UP WEB FOLDER"
#zip -rqy  $BACKUPS_DIR/$BACKUP_STAMP\.web.zip web/    ||  exit 1
tar cfzp  $BACKUPS_DIR/$BACKUP_STAMP\.web.tar.gz web/    ||  exit 1
echo "BACKING UP VENDOR FOLDER"
#zip -rqy  $BACKUPS_DIR/$BACKUP_STAMP\.vendor.zip vendor/    ||  exit 1
tar cfzp  $BACKUPS_DIR/$BACKUP_STAMP\.vendor.tar.gz vendor/    ||  exit 1
echo 
##### FINISH BACKUP


# RESTORE DATABASE:
# cat $BACKUPS_DIR/sirs-drupal--2020-02-25.18_55_44--540f5a6370a45dd7a7c02d2d20943a1d7b958c87.db.sql  | drush sql-cli
exit 0
