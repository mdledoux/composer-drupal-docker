#!/bin/bash
source .env
docker run -d --name $CONTAINER_CACHE \
	--rm \
	--network $NETWORK \
	memcached

	#INSTEAD OF --rm:
	#--restart 


#docker run --name my-memcache -d memcached
#docker run --name my-memcache -d memcached memcached -m 64
