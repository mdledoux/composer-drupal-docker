#!/bin/bash
source .env
source env.include.sh

docker run --rm  \
	-p 8080:80 -p 4433:443 \
	-v $PWD/drupal-project:/var/www  -v $PWD/logs/apache2:/var/logs/apache2   \
	-v $PWD/persist/web/home:/root \
	--network $NETWORK \
	--name $CONTAINER_WEB -d drupal:latest


