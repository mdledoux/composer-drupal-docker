#!/bin/bash
echo
echo "RUNNING site-revert.sh !!!!"
source env.include.sh
source $BASE_PATH/.env


PROJECT_NAME=$(basename "$PWD")

# CHECK FOR FIRST ARGUMENT
if [ -z "$1" ]; then
  echo "You must specify a backup location, followed by a base filename:"
  echo "  Usage:  $0 /web/backups    project-DATE-COMMIT"
  echo "ONLY ABSOLUTE PATHS ALLOWED - AVOID THIS:"
  echo "  Usage:  $0 ../backups    project-DATE-COMMIT"
  echo
  exit 1
else
  BACKUPS_DIR=$1
  echo "BACKUPS DIR: $1"
fi

# CHECK FOR SECOND ARGUMENT
if [ -z "$2" ]; then
  echo "You must specify a base-filename of the backup (including project-dir, date/time and  Git commit HASH), in addition to your backup location:"
  echo "  Usage:  $0 /web/backups    project-DATE-COMMIT"
  echo
  exit 1
else
  BACKUP_STAMP=$2
  echo "BACKUP FILES STAMP: $2"
fi




echo
echo
echo ROLLING BACK THE DATABASE
echo
echo
# RESTORE DATABASE - you may get prompted for a DB password:
# cat $BACKUPS_DIR/sirs-drupal--2020-02-25.18_55_44--540f5a6370a45dd7a7c02d2d20943a1d7b958c87.db.sql  | drush sql-cli
(cat $BACKUPS_DIR/$BACKUP_STAMP\.db.sql  |  NO_TTY=true BAK_FILE_PATH=$BACKUPS_DIR  $DRUSH sql-cli)  ||  (echo "DB-restore failed - QUITING (skipping folder restore)"  &&    exit 1)
# head $BACKUPS_DIR/$BACKUP_STAMP\.db.sql   

$DRUPAL cr


exit 0
