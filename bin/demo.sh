#!/bin/bash
source env.include.sh #figure out path for this
source .env
echo

#CMD=$(basename "$0")
DRUPAL_PROJECT_PATH=$BASE_PATH/$DRUPAL_PROJECT

if [[ $DEVMODE == true ]]; then
echo
echo
echo "OLD_PATH: $OLD_PATH"
echo "    PATH: $PATH"
echo

echo "              ARG[0]: $0"
echo "                CMD: $CMD"
echo "          BASE_PATH: $BASE_PATH"
echo "          BASE_NAME: $BASE_NAME"
echo "      CONTAINER_WEB: $CONTAINER_WEB"
echo "DRUPAL_PROJECT_PATH: $DRUPAL_PROJECT_PATH"
echo
echo "DB_DRVR: $DB_DRVR"
echo "DB_HOST: $DB_HOST"
echo "DB_PORT: $DB_PORT"
echo "DB_NAME: $DB_NAME"
echo "DB_USER: $DB_USER"
echo "DB_PASS: $DB_PASS"
echo
fi
