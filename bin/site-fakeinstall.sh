#!/bin/bash 
sudo echo
source env.include.sh
#--debug
source .env  #needed in case DRUPAL_PROJECT dir is ovverriden in .env
source passwords.env  #needed in case DRUPAL_PROJECT dir is ovverriden in .env

sudo cp -p  $TOOL_PATH/home/* ./persist/web/home/


echo
#echo "NOW RUNNING test (script inside of web container's /root)"
#COMMAND="$DOCKER exec -it $CONTAINER_WEB  bash -c 'bash /root/test.sh'"
#echo $COMMAND
#eval $COMMAND

echo
#echo "NOW RUNNING site_install (script inside of web container's /root)"
#COMMAND="$DOCKER exec -it $CONTAINER_WEB  bash -c 'bash /root/site_install_web_container.sh'"
#echo $COMMAND
#eval $COMMAND

DB_NAME=$POSTGRES_DB$MYSQL_DATABASE
DB_USER=$POSTGRES_USER$MYSQL_USER
DB_PASS=$POSTGRES_PASSWORD$MYSQL_PASSWORD
DB_PORT=$POSTGRES_PORT$MYSQL_PORT

if [ $DB_DRVR == "pgsql" ]; then
  DB_PORT_INTERNAL=5432
elif [ $DB_DRVR == "mysql" ]; then
  DB_PORT_INTERNAL=3306
fi


echo "DRUPAL_PROJECT: $DRUPAL_PROJECT"
SITE_INSTALL_TEXT=" \
\$databases['default']['default'] = array ( \
  'database' => '$DB_NAME', \
  'username' => '$DB_USER', \
  'password' => '$DB_PASS', \
  'prefix' => '', \
  'host' => 'db', \
  'port' => '$DB_PORT_INTERNAL', \
  'namespace' => 'Drupal\\\\Core\\\\Database\\\\Driver\\\\$DB_DRVR', \
  'driver' => '$DB_DRVR', \
); \
"

SETTINGS_PHP=$DRUPAL_PROJECT/web/sites/default/settings.php
sudo cp $DRUPAL_PROJECT/web/sites/default/default.settings.php  $SETTINGS_PHP
sudo mkdir -p $DRUPAL_PROJECT/web/sites/default/files
sudo echo >> $SETTINGS_PHP
sudo echo "\$settings['hash_salt'] = 'L5YIfBRvhox8tT-qKVntw_YfQh9RtHcugGGe4CuiobEQOOtO0pB09fRyMTUG6i84AU5AsLjtLw';" >> $SETTINGS_PHP
sudo echo >> $SETTINGS_PHP 
sudo echo $SITE_INSTALL_TEXT >> $SETTINGS_PHP 
sudo echo >> $SETTINGS_PHP



echo $SITE_INSTALL_TEXT

if [ $WINDOWS -eq 0 ]; then
    echo
    sudo chown 1000:www-data drupal-project/web/sites/default/files
    sudo chmod -R 777 drupal-project/web/sites/default/files
    sudo chmod -R a-w drupal-project/web/sites/default/settings.php
fi

