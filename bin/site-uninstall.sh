#!/bin/bash
source env.include.sh

sudo chmod -R u+w drupal-project/web/sites/default/
sudo rm -f drupal-project/web/sites/default/settings.php
sudo rm -rf drupal-project/web/sites/default/files

# SQLite
sudo rm -rf drupal-project/web/sites/default/database

