#!/bin/bash
source env.include.sh
#doesn't work: alias docker='winpty docker'

# COPY FILES FOR DOCKER AND ENV FILES
FILES=(
        docker-compose.mysql.yml
        docker-compose.pgsql.yml
        docker-compose.sqlite.yml
        docker-compose.override.yml
        #docker-compose.override.yml.SAMPLE
        docker-compose.yml
        drupal.Dockerfile
        composer.Dockerfile
	.dockerignore
	gitignore
        .env.SAMPLE_MY
        .env.SAMPLE_PG
        .env.SAMPLE_SL
        .gitlab-ci.yml
        passwords.env.SAMPLE
)



echo
for file in "${FILES[@]}" ; do    #print the new array
	echo "$file"
	# DISABLE COPY:
	cp $TOOL_PATH/$file $BASE_PATH
done

mv $BASE_PATH/gitignore $BASE_PATH/.gitignore 
#cd $BASE_PATH; git checkout .gitignore
echo

exit 0
