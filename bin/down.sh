#!/bin/bash
source env.include.sh
echo
cd $BASE_PATH


CMD=$(basename "$0")
source .env


docker-compose -f docker-compose.yml -f docker-compose.$DB_DRVR.yml  -f  docker-compose.override.yml  down   --remove-orphans
#docker-compose down

echo
echo "To reinitialize the database, use"
if [ $DB_DRVR == "pgsql" ]; then
	echo "    docker volume rm ${BASE_NAME}_pgdata"
	echo "followed by"
elif [ $DB_DRVR == "mysql" ]; then
	echo "    docker volume rm ${BASE_NAME}_mydata"
	echo "followed by"
fi
echo "site-uninstall.sh"
echo
