#!/bin/bash

source .env

docker stop $CONTAINER_WEB
docker stop $CONTAINER_CACHE
docker stop $CONTAINER_DB

docker network rm $NETWORK
