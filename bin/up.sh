#!/bin/bash
echo
source env.include.sh
echo
cd $BASE_PATH


CMD=$(basename "$0")
source .env || exit 1


docker-compose -f docker-compose.yml -f docker-compose.$DB_DRVR.yml  -f  docker-compose.override.yml  up  --build &
# DETACHED mode doesn't seem to work - should return the names of the containers
#docker-compose -d -f docker-compose.yml -f docker-compose.$DB_DRVR.yml  up

#PERMISION DENIED at this point - will try to put this into site-install scripts
#sleep 5
#cp home/composer_bashrc persist/web/home/.bashrc
