#!/bin/bash
sudo echo
echo
echo "RUNNING site-revert.sh !!!!"
source env.include.sh
source $BASE_PATH/.env


PROJECT_NAME=$(basename "$PWD")

# some good logic to use:
# mkdir $1 && cd $1 || exit 1



# CHECK FOR FIRST ARGUMENT
if [ -z "$1" ]; then
  echo "You must specify a backup location, followed by a base filename:"
  echo "  Usage:  $0 /web/backups    project-DATE-COMMIT"
  echo "ONLY ABSOLUTE PATHS ALLOWED - AVOID THIS:"
  echo "  Usage:  $0 ../backups    project-DATE-COMMIT"
  echo
  exit 1
else
  BACKUPS_DIR=$1
  echo "BACKUPS DIR: $1"
fi

# CHECK FOR SECOND ARGUMENT
if [ -z "$2" ]; then
  echo "You must specify a base-filename of the backup (including project-dir, date/time and  Git commit HASH), in addition to your backup location:"
  echo "  Usage:  $0 /web/backups    project-DATE-COMMIT"
  echo
  exit 1
else
  BACKUP_STAMP=$2
  echo "BACKUP FILES STAMP: $2"
fi




# RESTORE DATABASE 
site-restore.sh $@

echo
echo
echo "RESTORING FILES (web and vendor folders)"
echo "swtiching from: " `pwd` 
cd $BASE_PATH/$DRUPAL_PROJECT
echo "            to:  $BASE_PATH/$DRUPAL_PROJECT"
echo "(all you should see is \"$DRUPAL_PROJECT\" appended to the first path)"
echo
echo
# PRESERVE CURRENT POTENTIALLY BAD FILES
mv web web.POST_UPDATED.$BACKUP_STAMP || exit 1
mv vendor vendor.POST_UPDATED.$BACKUP_STAMP || exit 1
# RESTORE FILES - requires SUDO to preserve attributes and ownership
#unzip -q $BACKUPS_DIR/$BACKUP_STAMP.vendor.zip
sudo tar xfz $BACKUPS_DIR/$BACKUP_STAMP\.vendor.tar.gz  ||  exit 1

#unzip -q $BACKUPS_DIR/$BACKUP_STAMP.web.zip
sudo tar xfz $BACKUPS_DIR/$BACKUP_STAMP\.web.tar.gz  ||  exit 1
echo ">>> SUDO: chown -R apache:users web/;   chmod g+w web/sites/default"

sudo chown -R www-data:users web/sites/default/files
#sudo chmod g+w web/sites/default  && \
#sudo chown -R apache:users web/sites/default/files  && \
(
sudo chmod  g+w web/sites/default  && \
sudo chown -R apache:users web/sites/default/files && \
sudo chcon chcon -R -t httpd_sys_content_t ./vendor  && \
sudo chcon -R -t httpd_sys_content_rw_t ./web/sites/default  && \
sudo chcon -R -t httpd_sys_content_t ./config  \
) || exit 1
#sudo chcon -R -t httpd_sys_content_t ./load.environment.php   && \

exit 0
