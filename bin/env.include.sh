#!/bin/sh
source upsearch
DEVMODE=false
DRUPAL=drupal 
DRUSH=drush
COMPOSER=composer

CMD=$(basename "$0")
BASE_PATH=${0//bin\/$CMD/}
BASE_NAME=$(basename "$BASE_PATH")
TOOL_PATH=${0//bin\/$CMD/}
DRUPAL_PROJECT=drupal-project
#DRUPAL_PROJECT=drupal
#DRUPAL_PROJECT_PATH=$BASE_PATH/$DRUPAL_PROJECT # taken from demo.sh # DO NOT USE THIS IN env.include.sh:  it is intended to be used only in scripts AFTER create-project has run.
DRUPAL_PROJECT_PATH=${0//bin\/$CMD/$DRUPAL_PROJECT}
DRUPAL_TEMPLATE=drupal-composer/drupal-project:8.x-dev
#DRUPAL_TEMPLATE=drupal/recommended-project
#for Windows systems that can't symlink web to html
WEB_FOLDER=html


#BASE_PATH=$PWD; echo "BASE_PATH override - now PWD: $BASE_PATH"
BASE_PATH=`upsearch passwords.env`   ||  exit 1; $DEVMODE && echo "BASE_PATH override - now project parent folder of current dir: $BASE_PATH"
if [[ "$BASE_PATH" == "" ]]; then
	echo "NO BASE PATH FOUND:  Using CURRENT PATH:  $PWD"
	BASE_PATH=$PWD
fi
BASE_NAME=$(basename "$BASE_PATH")
DRUPAL_PROJECT_PATH=$BASE_PATH/$DRUPAL_PROJECT


unameOut="$(uname -s)"
if [[ "$unameOut" == *"MINGW"* ]]; then
    #Windows
    DOCKER='winpty docker'
    WINDOWS=1
    CONTAINER_BASE_NAME=$BASE_NAME
elif [[ "$unameOut" == *"Darwin"* ]]; then
    #Mac
    DOCKER='docker'
    WINDOWS=0
    USER_PARAM="--user $(id -u):$(id -g)"
    CONTAINER_BASE_NAME=$BASE_NAME
else
    #Linux
    DOCKER='docker'
    WINDOWS=0
    USER_PARAM="--user $(id -u):$(id -g)"
    #echo "BASE_NAME: $BASE_NAME"
    	#older versions of docker-compose required this:
    #CONTAINER_BASE_NAME=${BASE_NAME//_}
    #CONTAINER_BASE_NAME=${CONTAINER_BASE_NAME//-}
    CONTAINER_BASE_NAME=$BASE_NAME
fi


#COMPOSER_IMAGE=ciandtchina/composer-drupal
COMPOSER_IMAGE=composer-drupal-custom # && $DOCKER image build -t $COMPOSER_IMAGE $TOOL_PATH && echo && echo

echo "CONTAINER_BASE_NAME: $CONTAINER_BASE_NAME"
if [[ "$CONTAINER_BASE_NAME" != "" ]]; then
	CONTAINER_WEB=`docker ps --format "{{.Names}}" | grep $CONTAINER_BASE_NAME\_web`
	#CONTAINER_NUMBER=1 # need to work out a way to handlem more than one instance of a site...odd chance though, since basename would need ot be the same
	CONTAINER_DB=`docker ps --format "{{.Names}}" | grep $CONTAINER_BASE_NAME\_db`
	PGDATA_VOLUME=`docker volume ls -q | grep $CONTAINER_BASE_NAME\_pgdata`
	CONTAINER_NETWORK=`docker network ls --format "{{.Name}}" | grep $CONTAINER_BASE_NAME`
fi


$DEVMODE && echo "COMMAND:" $CMD $0
$DEVMODE && echo "DOCKER COMMAND:" $DOCKER
$DEVMODE && echo "COMPOSER_IMAGE:" $COMPOSER_IMAGE
$DEVMODE && echo "PWD:" $PWD
$DEVMODE && echo "BASE_PATH:" $BASE_PATH
$DEVMODE && echo "TOOL_PATH:" $TOOL_PATH
$DEVMODE && echo "BASE_NAME:" $BASE_NAME
$DEVMODE && echo "CONTAINER_WEB:" $CONTAINER_WEB
$DEVMODE && echo "CONTAINER_DB:" $CONTAINER_DB
$DEVMODE && echo "PGDATA_VOLUME:" $PGDATA_VOLUME
$DEVMODE && echo "DRUPAL_PROJECT_PATH:" $DRUPAL_PROJECT_PATH
$DEVMODE && echo "ARGS:" $@


