#!/bin/bash 
source env.include.sh
#--debug
#source .env  #not needed anymore with env.include.sh? put back to allow overrides


if [ $WINDOWS -eq 0 ]; then
    sudo echo
    sudo chown 1000:www-data logs
    sudo chmod -R 777 logs
    sudo chown 1000:www-data persist/web/home
    sudo chmod -R 777 persist/web/home
fi

cp -p  $TOOL_PATH/home/* ./persist/web/home/


echo
echo "NOW RUNNING test (script inside of web container's /root)"
COMMAND="$DOCKER exec -it $CONTAINER_WEB  bash -c 'bash /root/test.sh'"
echo $COMMAND
eval $COMMAND

echo
echo "NOW RUNNING site_install (script inside of web container's /root)"
COMMAND="$DOCKER exec -it $CONTAINER_WEB  bash -c 'bash /root/site_install_web_container.sh'"
echo $COMMAND
eval $COMMAND

if [ $WINDOWS -eq 0 ]; then
    sudo chown 1000:www-data drupal-project/web/sites/default/files
    sudo chmod -R 777 drupal-project/web/sites/default/files
fi

