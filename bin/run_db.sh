#!/bin/bash
source .env
docker run -d --name $CONTAINER_DB \
	--rm \
	--network $NETWORK \
	-e POSTGRES_DB=$DB_NAME \
	-e POSTGRES_USER=$DB_USER \
	-e POSTGRES_PASSWORD=$DB_PASS \
	postgres:11

	#INSTEAD OF --rm:
	#--restart 
