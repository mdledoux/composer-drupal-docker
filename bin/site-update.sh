#!/bin/bash
echo
echo "RUNNING site-update.sh !!!!"
source env.include.sh
source $BASE_PATH/.env

CMD=$(basename "$0")
TOOL_PATH=${0//bin\/$CMD/}
TOOL_PATH=${0//$CMD/}
# THESE ABOVE ARE NECESSAY BECAUSE env.include.sh IS NOT SOURCING...


# BACKUP PHASE
site-backup.sh $@
##### FINISH BACKUP
echo "FINISHED BACKUP"
echo "TOOL_PATH: $TOOL_PATH"



echo "TURNING ON MAINTENANCE MODE"
$DRUPAL sma on  ||  exit 1


echo "PULLING LATEST CODE"
git pull   ||  (echo "GIT PULL FAILED" &&  exit 1)
echo

echo "RUNNING COMPOSER INSTALL"; echo
#($COMPOSER install   &&   true)    ||    ( (echo "COMPOSER install FAILED" &&  $TOOL_PATH/revert_site.sh    $BACKUPS_DIR  $BACKUP_STAMP)  &&   exit 1)
$COMPOSER install    ||    ( echo "COMPOSER install FAILED - EXITING:  you may need to manually restore the web and vendor folders if the site is not working!"   &&   exit 1)
echo 
echo "****** COMPOSER INSTALL COMPLETED SUCCESSFULLY *******"
echo
echo




echo "UPDATING DATABASE"
# OLD: drush updatedb
(true && $DRUPAL updb) || ( (echo "An error occured updating the DB: reverting site...."  &&        $TOOL_PATH/revert_site.sh    $BACKUPS_DIR  $BACKUP_STAMP)    || exit 1)
#echo "UPDATING ENTITIES"
#$DRUPAL upe || (echo "An error occured updating the Entities" && exit 1)


echo "TURNING OFF MAINTENANCE MODE"
$DRUPAL sma off || (echo "An error occured turning OFF Maintenance Mode" && exit 0)


exit 0
