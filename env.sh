# source this - do not execute as a shell-script script!
# this sets up the environment to run the composer docker-container as a local command

#source .env


echo "Setting paths...."
OLD_PATH=$PATH
PATH=$PWD/bin:$PATH

set OLD_PATH=$PATH
set PATH=$PWD/bin:$PATH
echo "OLD_PATH: $OLD_PATH"
echo "    PATH: $PATH"



source env.sh.local.sh
