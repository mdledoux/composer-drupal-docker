# Setting up Drupal sites with Composer via Docker

## Getting Started

### First steps
1. Install Docker
***If you are running this on Windows, be sure to follow the instructions below:  "Installing on Windows"***
2. Make sure this repo is in the PATH.  Go to installed location (e.g., /opt/composer-drupal-docker/) and run ```source env.sh``` to manually add it to the PATH.  At any time you can go to the composer-drupal-docker directory and type ```reset_path.sh``` to unset that PATH, but **be careful about running env.sh more than once** - it wil incorreclty mess up your preserved "OLD_PATH" and put the location of the project into your PATH twice.

### Creating a Drupal Site
1. run: $``` create-project my_project_name ```
2. This will create a project folder with the name ***my_project_name*** and put some files in there and install Drupal into a sub-folder called drupal-project.
   When it is done, it will end by showing you where to go (as a reminder, as it might have taken a while and scrolled off your screen):
```
         New project now in:  my_project_name
         cd my_project_name
```
3. Go into the folder:  ```cd my_project_name```.   Copy two files to get started:   ``` cp passwords.env.SAMPLE passwords.env ``` and ```cp .env.SAMPLE_PG .env ``` (for PostGreSQL - use SAMPLE_MY for MySQL/MariaDB)
4. If desired, edit the passwords in the passwords.env file, or change any other values, like the name of the site in .env (SITE_NAME is the variable that will be used by the site installation script coming up...).  Spaces are allowed - no quotes.  You may also want to change WEB_PORT - if you're bringing up more than one site, only one can come up on the default, 8080.  Adjust accordingly if you have multiple sites.
5. Start the services (web, database, caching, network):   ``` up.sh ``` - **if you used a space in your SITE_MAME variable, you will see a benign error**.
6. Run ``` site-install.sh ```.  If all is good, this should fire off amd run for a few minutes.  **If you run this in a new terminal, you will see logging in the window that you ran ``` up.sh ``` in**. (if these tools are NOT in your PATH, remember to ``` source env.sh ``` in the composer-drupal-docker directory).
7. Your site is installed!  Navigate to
8. If necessary, feel free to make a default master module (a non-shared module that will be unique to this site).  The command is simple: $``` make_module.sh ```
```
 Enter the new module name:
 > Master Module
 Enter the module machine name [master_module]:
 >
 Enter module description [My Awesome Module]:
 >
 Would you like to add module dependencies? (yes/no) [no]:
 >
 Do you want proceed with the operation? (yes/no) [yes]:
 >
```
8. 







### Installing on Windows

Docker installation on Windows is a bit of a complex process - this set of instructions ought to
work for most Windows 10 installations, but you might be forced to troubleshoot issues as they occur beyond that,
on your own.

The following steps are required to install Docker on windows:

1.  Download Docker Desktop for Windows [https://hub.docker.com/editions/community/docker-ce-desktop-windows] - DO NOT run install as Administrator
2.  After the install is completed, you'll need to run a command shell, then type in ```whoami``` and \
note your username (it'll be ```DOMAIN\FIRSTNAME.LASTNAME```)
3.  Open a cmd shell as administrator, then enter the following command:
    - ```net localgroup docker-users DOMAIN\FIRSTNAME.LASTNAME /add```
    - Replace ```DOMAIN/FIRSTNAME.LASTNAME``` ```with your username from step 3.
4.  If you already rebooted after installing docker, then you will need to login and logout for the group change to take effect.
5.  Mounting local volumes with Docker compose will not work until you have done the following:
    - Got to: Local Security Policy > Network List Manager Policies
    - Double-click on "unidentified Networks", change the location type to "private" 
    - Restart docker if it is already started.
6.  Launch docker desktop, and wait for it to start (it will take some time to launch.)
7.  Go to docker desktop settings, choose "Resources" and set your MEMORY resources to roughly 50% of the RAM on your system.
